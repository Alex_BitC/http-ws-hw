import { Server, Socket } from 'socket.io';
import { User } from '../utils/user';
import UserList from '../utils/user-lists';
import RoomList from '../utils/room-list';
import { Room } from '../utils/room';
import * as config from './config';

export default (io: Server) => {
	io.on('connection', socket => {
		const username = socket.handshake.query.username as string;

		const activeUser = new User(username, socket.id);

		if (UserList.isExistActiveUser(activeUser)) {
			socket.emit('ACTIVE_USER_EXISTS', username);
		} else {
			UserList.addUser(activeUser);
			socket.emit('UPDATE_ROOMS', RoomList.rooms);
		}

		socket.on('disconnect', () => {
			UserList.deleteUserById(socket.id);
		});

		const joinRoom = (roomName: string) => {

			const indexRoom = RoomList.getIndex(roomName);
			if (!RoomList.rooms[indexRoom].addUser(activeUser)) {
				socket.emit('TOO_MANY_USERS');
				return;
			}

      const activeRoom = RoomList.getRoomByName(roomName);
      socket.join(roomName);

			io.to(socket.id).emit('JOIN_ROOM_DONE', activeRoom);
			io.emit('UPDATE_ROOMS', RoomList.rooms);

		}

		socket.on('CREATE_ROOM', (roomName: string) => {
			const newRoom = new Room(roomName);
			if (RoomList.isSameRoom(newRoom)) {
				socket.emit('SAME_ROOM_EXIST', roomName);
				return;
			}
			RoomList.addRoom(newRoom);
			joinRoom(roomName);
		});

		socket.on("JOIN_ROOM", joinRoom);

		const leaveRoom = (roomName: string) => {
			socket.leave(roomName);
			const indexRoom = RoomList.getIndex(roomName);

			console.log('deleted, ', indexRoom, roomName, RoomList)
			RoomList.rooms[indexRoom].deleteUser(activeUser);
			if (!RoomList.rooms[indexRoom].userCount) {
				RoomList.deleteRoom(roomName);
			}
			io.emit('UPDATE_ROOMS', RoomList.rooms);
		}

		socket.on("EXIT_ROOM", leaveRoom);




	});
};
