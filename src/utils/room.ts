import { IRoom } from './i-room';
import { User } from './user';
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../socket/config';

export class Room implements IRoom {
  readonly name: string;
  users: User[];
  readyStatus: boolean;
  activeUser: User | null;
  userCount: number;

  constructor(name: string ) {
    this.name = name;
    this.users = [];
    this.readyStatus = false;
    this.activeUser = null;
    this.userCount = this.getUserCount();
  }

  addUser = (user: User): boolean => {
    if (this.isFull()) {
      return false;
    }
    this.users.push(user);
    this.userCount = this.getUserCount();
    return true;
  }

  isFull = () => {
    return this.getUserCount() >= MAXIMUM_USERS_FOR_ONE_ROOM;
  }

  setActiveUser( user: User) {
    this.activeUser = user;
  }

  getUserCount = () => {
    return this.users.length;
  }

  isUserInRoom = (user: User): boolean => {
    return !!this.users.find(currentUser => currentUser.username === user.username);
  }

  deleteUser = (user: User) => {
    const removedUser = this.users.find(currentUser => currentUser.username === user.username);
    const deleteIndex = this.users.indexOf(removedUser!);
    this.users.splice(deleteIndex, 1);

    this.userCount = this.getUserCount();
  }
}
