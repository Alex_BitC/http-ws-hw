import { User } from './user';

export interface IRoom {
  name: string,
  users: User[],
  readyStatus: boolean,
  activeUser: User | null
}