import { IUser } from './i-user';

export class User implements IUser {
  readonly username: string;
  readonly socketId: string;
  isReady: boolean;


  constructor( username: string, socketId: string) {
    this.username = username;
    this.socketId = socketId;
    this.isReady = false;
  }
}
