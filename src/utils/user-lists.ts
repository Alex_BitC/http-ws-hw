import { User } from './user';

class UserList {
  private readonly users: User[];
  constructor() {
    this.users = [];
  }

  addUser = (user: User) => {
    this.users.push(user);
  }

  isExistActiveUser = (user: User): boolean => {
    return !!this.users.find(currentUser => {
      return currentUser.username === user.username && currentUser.socketId !== user.socketId;
    });
  }

  deleteUserById = (socketId: string) => {
    const currentUser = this.users.find(user => user.socketId === socketId);
    if (currentUser) {
      this.users.splice(this.users.indexOf(currentUser), 1);
    }
  }
}

export default new UserList;
