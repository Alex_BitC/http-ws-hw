export interface IUser {
  username: string,
  socketId: string,
  isReady: boolean
}
