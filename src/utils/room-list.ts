import { Room } from './room';

class RoomList {
  readonly rooms: Room[];
  constructor() {
    this.rooms = [];
  }

  addRoom = (room: Room) => {
    this.rooms.push(room);
  }

  getRoomByName = (roomName: string) => {
    return this.rooms.find(room => room.name === roomName);
  }

  getIndex = (roomName: string) => {
    return this.rooms.indexOf(this.getRoomByName(roomName)!);
  }

  isSameRoom = (room: Room): boolean => {
    return !!this.rooms.find(currentRoom => currentRoom.name  === room.name);
  }

  deleteRoom = (roomName: string) => {
    const deleteIndex = this.getIndex(roomName);
    this.rooms.splice(deleteIndex, 1);
  }



  // isExistActiveUser = (user: User): boolean => {
  //   return !!this.users.find(currentUser => {
  //     return currentUser.username === user.username && currentUser.socketId !== user.socketId;
  //   });
  // }

  // deleteUserById = (socketId: string) => {
  //   const currentUser = this.users.find(user => user.socketId === socketId);
  //   if (currentUser) {
  //     console.log('delete');
  //     this.users.splice(this.users.indexOf(currentUser), 1);
  //   }
  // }
}

export default new RoomList;
