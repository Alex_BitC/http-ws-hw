import { showInputModal, showMessageModal } from './views/modal.mjs';
import { appendRoomElement, updateNumberOfUsersInRoom, removeRoomElement } from './views/room.mjs';
import { removeClass, addClass } from './helpers/domHelper.mjs';
import { appendUserElement, changeReadyStatus, setProgress, removeUserElement } from './views/user.mjs';


const username = sessionStorage.getItem('username');

if (!username) {
	window.location.replace('/login');
}

const socket = io('', { query: { username } });

const handleActiveUserExists = (username) => {
	const message = `User: ${username} already exist! \nPlease, enter another username.`;

	const onClose = () => {
		window.location.replace('/login');
	};

	const congigActiveUserExists = {
		message: message,
		onClose: onClose
	}

	sessionStorage.clear();
	showMessageModal(congigActiveUserExists);
}

socket.on('ACTIVE_USER_EXISTS', handleActiveUserExists);

const createRoom = (roomName) => {
	socket.emit('CREATE_ROOM', roomName);
}

const handleAddRoomButton = () => {
	const title = 'Please, enter the name of the room:';

	let roomName;
	const onChange = (value) => {
		roomName = value;
	};

	const onSubmit = () => {
		if (!roomName) {
			return;
		}
		createRoom(roomName);
	};

	const configModalCreateRoom = {
		title: title,
		onChange: onChange,
		onSubmit: onSubmit
	};
	showInputModal(configModalCreateRoom);
}

const addRoomButton = document.querySelector('#add-room-btn');
addRoomButton.addEventListener('click', handleAddRoomButton);

const joinRoom = (roomName) => {
	socket.emit('JOIN_ROOM', roomName);
}

const transformToConfigCreateRoom = (room) => {
	const roomName = `Room Name: ${room.name}`;
	const numberOfUsers = room.userCount;

	const onJoin = () => {
		joinRoom(room.name);
	}

	return {
		name: roomName,
		numberOfUsers: numberOfUsers,
		onJoin: onJoin
	}
}

const updateRooms = rooms => {
	const roomsContainer = document.querySelector('#rooms-wrapper');
	const allRooms = rooms.map(transformToConfigCreateRoom);
	roomsContainer.innerHTML = "";
	allRooms.forEach(appendRoomElement);
};

socket.on('UPDATE_ROOMS', updateRooms);

const handleSameRoomExist = (roomName) => {
	const message = `Room: ${roomName} already exist! \nPlease, enter another name.`;

	const onClose = () => {}

	const congigActiveUserExists = {
		message: message,
		onClose: onClose
	}

	sessionStorage.clear();
	showMessageModal(congigActiveUserExists);
}

socket.on('SAME_ROOM_EXIST', handleSameRoomExist);

const handleTooManyUsers = () => {
	const message = `Too many users!`;

	const onClose = () => {}

	const congigActiveUserExists = {
		message: message,
		onClose: onClose
	}

	sessionStorage.clear();
	showMessageModal(congigActiveUserExists);
}

socket.on('TOO_MANY_USERS', handleTooManyUsers);

const showGamePage = () => {
	const roomsPage = document.querySelector('#rooms-page');
	addClass(roomsPage, 'display-none');
	const gamePage = document.querySelector('#game-page');
	removeClass(gamePage, 'display-none');
}

const showRoomsPage = () => {
	const roomsPage = document.querySelector('#rooms-page');
	removeClass(roomsPage, 'display-none');
	const gamePage = document.querySelector('#game-page');
	addClass(gamePage, 'display-none');
}

const clearPageGame = () => {
	const RoomName = document.querySelector('#room-name');
	RoomName.innerHTML = '';
	const usersContainer = document.querySelector('#users-wrapper');
	usersContainer.innerHTML = '';

}

const transformToConfigAppendUserElement = (user) => {

	return {
		username: `username #${user.username}`,
		ready: user.isReady,
		isCurrentUser: user.username === username
	}
}


const handleJoinRoomDone = (activeRoom) => {
	showGamePage();
	const QuitRoomBt = document.querySelector('#quit-room-btn');
	QuitRoomBt.addEventListener('click', () => {
		showRoomsPage();
		clearPageGame();
		socket.emit('EXIT_ROOM', activeRoom.name);
	});
	const RoomName = document.querySelector('#room-name');
	RoomName.innerHTML = `Room name: #${activeRoom.name}`;
	const configAppendUserElement = activeRoom.users.map(transformToConfigAppendUserElement);
	configAppendUserElement.forEach(appendUserElement);


}


socket.on('JOIN_ROOM_DONE', handleJoinRoomDone);
